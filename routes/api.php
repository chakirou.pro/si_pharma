<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Medicament;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/** CRUD medicament */
//api route : http://127.0.0.1:8000/api/medicament/store
Route::post('/medicament/store', 'MedicamentController@store');

//api route : http://127.0.0.1:8000/api/medicament/update/{id}
Route::post('/medicament/update/{id}', 'MedicamentController@update');

// api route : http://127.0.0.1:8000/api/medicament/index
Route::get('/medicament/index', 'MedicamentController@index');

// api route : http://127.0.0.1:8000/api/medicament/show/{id}
Route::get('/medicament/show/{id}', 'MedicamentController@show');
