<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EtatMedicament extends Model
{
    //
    protected $guarded = [];

    const ACTIF = 1;
    const INACTIF = 2;
    const SUPPRIME = 3;
}
