<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicament;
use App\EtatMedicament;

class MedicamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $all_medoc = Medicament::all();

        if($all_medoc != null) {
            return response()->json([
                'success' => true,
                'all_medicaments' => $all_medoc,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Aucun produit enregistrer",
            ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Creer un nouveau medoc
        /**
         * required :
         * ------------
         * nom_medicament
         * nom_generic
         * prix_achat
         * taille_boit
         * description
         * description
         * force
         *
         * optional :
         * -------------
         * image
         * prix_achat
         *
         */

        $medoc = Medicament::create([
            'nom' => $request->nom_medicament,
            'nom_generic' => $request->nom_generic,
            'prix_vente' => $request->prix_vente,
            'prix_achat' => $request->prix_achat,
            // 'image' => $request->nom_medicament,
            'taille_boite' => $request->taille_boite,
            'description' => $request->description,
            'force' => $request->force,
            // 'etat' => EtatMedicament::ACTIF,
        ]);

        /**store image & update the ur */
        if ($request->file('photo') != null) {
            $ext = $request->file('photo')->extension();
            $fileName = "img_".$request->nom_medicament."_".$medoc->id.".".$ext;
            $path = $request->file('photo')->move(public_path("/images/products/"), $fileName);
            $imageUrl = url('/images/products/'.$fileName);

            //Update image Url
            $medoc->update([
                'image' => $imageUrl,
            ]);
        }

        if($medoc != null ) {
            return response()->json([
                'success' => true,
                'new_medicament' => $medoc,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Erreur lors de la creation!",
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $medoc = Medicament::find($id);

        if($medoc != null ) {
            return response()->json([
                'success' => true,
                'medicament' => $medoc,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => "Ce medicament n'existe pas !",
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Retrieve the medoc
        $old_medoc = Medicament::find($id);

        $data = ([
            'nom' => $request->nom_medicament,
            'nom_generic' => $request->nom_generic,
            'prix_vente' => $request->prix_vente,
            'prix_achat' => $request->prix_achat,
            'taille_boite' => $request->taille_boite,
            'description' => $request->description,
            'force' => $request->force,
            // 'etat' => EtatMedicament::ACTIF,
        ]);

        if($old_medoc != null) {

            if($data != null) {
                Medicament::where('id', $id)->update($data);
                $updated_med = Medicament::where('id', $id)->first();

                if ($request->file('photo') != null) {
                    $ext = $request->file('photo')->extension();
                    $fileName = "img_".$request->nom_medicament."_".$old_medoc->id.".".$ext;
                    $path = $request->file('photo')->move(public_path("/images/products/"), $fileName);
                    $imageUrl = url('/images/products/'.$fileName);

                    //Update image Url
                    $updated_med->update([
                        'image' => $imageUrl,
                    ]);
                }

                return response()->json([
                    'success' => true,
                    'old_medoc' => $old_medoc,
                    'updated_med' => $updated_med,
                ]);

            } else {

                return response()->json([
                    'success' => false,
                    'message' => "Aucune donne mise à jour !",
                ]);

            }

        } else {
            return response()->json([
                'success' => false,
                'message' => "Ce medicament n'existe pas !",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    /**
     * Permet de supprimer un medicament
     * En fait on le change d'etat
     */
    public function stateChanging($id) {

    }
}
