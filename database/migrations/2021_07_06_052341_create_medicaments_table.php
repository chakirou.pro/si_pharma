<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicaments', function (Blueprint $table) {
            $table->id();
            $table->String('nom');
            $table->String('nom_generic');
            $table->integer('prix_achat');
            $table->integer('prix_vente')->nullable();
            $table->String('image')->nullable(); // Url de l'image
            $table->integer('taille_boite');
            $table->String('description');
            $table->String('force')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicaments');
    }
}
